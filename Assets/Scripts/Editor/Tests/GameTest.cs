﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PlasticGui.WebApi.Requests;
using Trivia;

namespace Editor.Tests
{
    public class GameTest
    {
        [Test]
        public void create_50_questions_for_each_category_when_created()
        {
            var game = new GameStub();
            var popQuestions = game.GetPopQuestions();
            var sportsQuestions = game.GetSportsQuestions();
            var scienceQuestions = game.GetScienceQuestions();
            var rockQuestions = game.GetRockQuestions();

            var popQuestionValues = popQuestions
                .Zip(Enumerable.Range(0, Game.AMOUNT_OF_QUESTIONS), 
                QuestionIndexIs);
            
            var sportQuestionValues = sportsQuestions
                .Zip(Enumerable.Range(0, Game.AMOUNT_OF_QUESTIONS), 
                    QuestionIndexIs);
            
            var scienceQuestionValues = scienceQuestions
                .Zip(Enumerable.Range(0, Game.AMOUNT_OF_QUESTIONS), 
                    QuestionIndexIs);
            
            var rockQuestionValues = rockQuestions
                .Zip(Enumerable.Range(0, Game.AMOUNT_OF_QUESTIONS), 
                    QuestionIndexIs);
            
            Assert.IsTrue(popQuestionValues.All(x => x), "All pop questions are set");
            Assert.IsTrue(scienceQuestionValues.All(x => x), "All science questions are set");
            Assert.IsTrue(sportQuestionValues.All(x => x), "All sports questions are set");
            Assert.IsTrue(rockQuestionValues.All(x => x), "All rock questions are set");
        }

        [Test]
        public void is_not_playable_when_has_does_not_have_at_least_2_players()
        {
            var game = new GameStub();
            game.Add("Javi");
            Assert.IsFalse(game.IsPlayable(), "Game must be no playable");
        }
        
        [Test]
        public void is_playable_when_has_at_least_2_players()
        {
            var game = new GameStub();
            game.Add("Javi");
            game.Add("Lauta");
            Assert.IsTrue(game.IsPlayable(), "Game is playable");
        }

        [Test]
        public void add_player_on_list_when_request()
        {
            var game = new GameStub();
            game.Add("Javi");
            Assert.IsTrue(game.GetPlayers().Contains("Javi"), "Add player successfully");
        }

        [Test]
        public void set_place_to_zero_when_add_player()
        {
            var game = new GameStub();
            game.Add("Javi");
            ThenPlaceIs(game, 1, 0);
        }

        [Test]
        public void set_purse_to_zero_when_add_player()
        {
            var game = new GameStub();
            game.Add("Javi");
            Assert.AreEqual(0, game.GetPurses()[1]);
        }
        
        [Test]
        public void init_penalty_box_when_add_player()
        {
            var game = new GameStub();
            game.Add("Javi");
            Assert.AreEqual(false, game.GetPenaltyBox()[1]);
        }

        [Test]
        public void increase_place_when_player_is_not_on_penalty_box()
        {
            var game = new GameStub();
            game.Add("Javi");
            game.Add("Lauta");
            game.WrongAnswer();
            game.Roll(3);
            ThenPlaceIs(game, 1, 3);
        }

        [Test]
        public void start_new_turn_when_complete_places()
        {
            var game = new GameStub();
            game.Add("Javi");
            game.Add("Lauta");

            game.SetPlayerPosition(1, 10);
            game.WrongAnswer();
            game.Roll(3);
            ThenPlaceIs(game, 1, 1);
        }

        [TestCase(3)]
        [TestCase(7)]
        [TestCase(11)]
        public void remove_rock_category_depending_on_place(int roll)
        {
            var game = new GameStub();
            game.Add("Javi");
            game.Add("Lauta");

            game.WrongAnswer();
            game.Roll(roll);

            foreach (var question in game.GetRockQuestions())
                ThenQuestionIsNotEqualTo(question, "0");
        }
        
        [TestCase(0)]
        [TestCase(4)]
        [TestCase(8)]
        public void remove_pop_category_depending_on_place(int roll)
        {
            var game = new GameStub();
            game.Add("Javi");
            game.Add("Lauta");

            game.WrongAnswer();
            game.Roll(roll);

            foreach (var question in game.GetPopQuestions())
                ThenQuestionIsNotEqualTo(question, "0");
        }
        
        [TestCase(1)]
        [TestCase(5)]
        [TestCase(9)]
        public void remove_science_category_depending_on_place(int roll)
        {
            var game = new GameStub();
            game.Add("Javi");
            game.Add("Lauta");

            game.WrongAnswer();
            game.Roll(roll);

            foreach (var question in game.GetScienceQuestions())
                ThenQuestionIsNotEqualTo(question, "0");
        }
        
        [TestCase(2)]
        [TestCase(6)]
        [TestCase(10)]
        public void remove_sport_category_depending_on_place(int roll)
        {
            var game = new GameStub();
            game.Add("Javi");
            game.Add("Lauta");

            game.WrongAnswer();
            game.Roll(roll);

            foreach (var question in game.GetSportsQuestions())
                ThenQuestionIsNotEqualTo(question, "0");
        }

        private void ThenPlaceIs(GameStub game, int playerBox, int expectedPlace)
        {
            Assert.AreEqual(expectedPlace, game.GetPlaces()[playerBox]);
        }

        private bool QuestionIndexIs(string popQuestion, int index) => popQuestion.Split(' ').Last() == index.ToString();

        private void ThenQuestionIsNotEqualTo(string question, string notEqualTo)
        {
            Assert.IsTrue(question.Split(' ').Last() != notEqualTo);
        }

        class GameStub : Game
        {
            public LinkedList<string> GetPopQuestions() => _popQuestions;

            public LinkedList<string> GetSportsQuestions() => _sportsQuestions;

            public LinkedList<string> GetScienceQuestions() => _scienceQuestions;

            public LinkedList<string> GetRockQuestions() => _rockQuestions;

            public List<string> GetPlayers() => _players;

            public int[] GetPlaces() => _places;

            public int[] GetPurses() => _purses;

            public bool[] GetPenaltyBox() => _inPenaltyBox;

            public void SetCurrentPlayer(int player) => _currentPlayer = player;

            public void SetPlayerPosition(int playerIndex, int position)
            {
                _places[playerIndex] = position;
            }
        }
    }
}